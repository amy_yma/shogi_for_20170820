class SyogiDef
  @syogi_lines = 9

  def self.direction
    [:forward,:backward]
  end

  def self.base_line
    ret = []
    self.direction.each {|dir|
      ret << %i/kyosha keima gin kin o kin gin keima kyosha/.map{|i|SyogiMan.new(i,dir)}
    }
    ret
  end

  def self.second_line
    ret = [Array.new(@syogi_lines,nil),Array.new(@syogi_lines,nil)]
    mans = %i/hisha kaku/
    dirs = %i/forward backward/
    ret.each do |i|
      i[1] = SyogiMan.new(mans[0] , dirs[0])
      i[@syogi_lines-2] = SyogiMan.new(mans[1] , dirs[0])
      mans.reverse!
      dirs.shift
    end
    ret
  end

  def self.third_line
    ret_arr = [[],[]]
    cnt = 0
    self.direction.each{|dir|
      @syogi_lines.times{ret_arr[cnt] << SyogiMan.new(:fu,dir)}
      cnt += 1
    }
    ret_arr
  end

  def self.line_num(x)
    [x,@syogi_lines - x - 1]
  end

  def self.outside?(x,y)
    !x.between?(0,@syogi_lines-1) || !y.between?(0,@syogi_lines-1)
  end

  def self.lines
    @syogi_lines
  end
end

class Syogi
  def initialize
    @board = []
    9.times{@board << Array.new(SyogiDef.lines,nil)}
    place_init #将棋の初期位置に駒を配置していく。
    @active = nil
    @player = []
    SyogiDef.direction.each{|i|@player << Player.new(i)}
    @playing = @player[0]
  end

  def display
    nums = %w"０ １ ２ ３ ４ ５ ６ ７ ８"
    cnt = -1
    print "  "
    nums.each{|i|print i}
    puts ""
    @board.each do |row|
      cnt += 1
      print nums[cnt]
      row.each do |item|
        if item.nil?
          print "._"
        else
          print "#{item.dispname}"
        end
      end
      puts ""
    end
  end

  def player?
    @playing.camp
  end

  def switch
    @playing = @player[@player.index(@playing) == 0 ? 1 : 0]
    player?
  end

  def activated?(x,y)
    !@active.nil?
  end

  def activate_man(x,y)
    return false if @board[x][y].nil? || SyogiDef.outside?(x,y)
    @active = [x,y] 
    @active != nil
    true
  end

  def place_init 
    SyogiDef.line_num(0).each{|i|@board[i] = SyogiDef.base_line[i == 0 ? 0 : 1]}
    line_num = SyogiDef.line_num(1)
    [0,1].each {|i| @board[line_num[i]] = SyogiDef.second_line[i]}
    SyogiDef.line_num(2).each{|i|@board[i] = SyogiDef.third_line[i == 2 ? 0 : 1]}
  end

  def has_man?(x,y)
    @board[x][y] == nil ? false : @board[x][y].name
  end

  def moveable_cell(x,y)
    target_man = @board[x][y]
    return nil if target_man.nil?
    arr = @board[x][y].moveable_cell
    ret_arr = []

    arr.each do |i|
      xdif = x + i[0] ; ydif = y + i[1]
      next if target_man.ally?(@board[xdif]&.[] ydif)

      ret_arr << [xdif , ydif]
      next unless target_man.enumerable?
      until SyogiDef.outside?(i[0] + xdif , i[1] + ydif)
        xdif += i[0] ; ydif += i[1]
        break if target_man.ally?(@board[xdif][ydif])
        ret_arr << [xdif , ydif]
        break if target_man.opponent?(@board[xdif][ydif])
      end
    end

    ret_arr.delete_if{|i|SyogiDef.outside?(i[0],i[1])}
    ret_arr
  end

  def moveable?(x,y)
    return false if SyogiDef.outside?(x,y)
    return false unless activated?(x,y)
    active_man = @board[@active[0]][@active[1]]
    return false unless moveable_cell(@active[0],@active[1]).include?([x,y])
    return false unless @playing.has?(active_man)
    true
  end

  def move!(x,y)
    return false unless moveable?(x,y)
    target_man = @board[x][y]
    target_man&.bitray
    @playing.pal_with(target_man)
    @board[x][y] = @board[@active[0]][@active[1]]
    @board[@active[0]][@active[1]] = nil
    @active = nil
    true
  end

  def sortie_man(x,y,man)
    return false if SyogiDef.outside?(x,y) || @board[x][y] != nil
    sortieing_man = @playing.sortie!(man)
    return false if sortieing_man.nil?
    @board[x][y] = sortieing_man
    true
  end

  def disp_pals
    ret = <<-EOS .gsub(/^\s+/, '')
      【持ち駒】
      先手：#{@player[0].disp_pals}
      後手：#{@player[1].disp_pals}
    EOS
  end
end

class SyogiMan
  attr_reader :name , :direction

  @@dispname = {
    fu: "歩",
    kyosha: "香",
    keima: "桂",
    gin: "銀",
    kin: "金",
    o: "王",
    hisha: "飛",
    kaku: "角",
  }
  @@enum = %i/hisha kaku kyosha/
  def initialize(name,direction)
    @name = name
    @direction = direction
  end

  def dispname
    @@dispname[@name]
  end

  def enumerable?
    @@enum.include?(@name)
  end

  def moveable_cell
    send((@name.to_s + "_moveable").to_sym)
  end

  def ally?(other_man)
    return false if other_man.nil?
    @direction == other_man.direction
  end

  def opponent?(other_man)
    return false if other_man.nil?
    @direction != other_man.direction
  end

  def bitray
    @direction = SyogiDef.direction[SyogiDef.direction.index(@direction) == 0 ? 1 : 0]
  end

  def fu_moveable
    [
      [vector , 0]
    ]
  end

  def o_moveable
    [
      [1,1],
      [1,0],
      [0,1],
      [-1,-1],
      [0,-1],
      [-1,0],
      [1,-1],
      [-1,1]
    ]
  end

  def kyosha_moveable
    [
      [vector , 0]
    ]
  end

  def keima_moveable
    [
      [vector * 2,1],
      [vector * 2,-1]
    ]
  end

  def gin_moveable
    [
      [vector , 0],
      [vector , 1],
      [vector , -1],
      [-vector , 1],
      [-vector , -1]
    ]
  end

  def kin_moveable
    [
      [vector , 0],
      [vector , 1],
      [vector , -1],
      [-vector , 0],
      [0 , 1],
      [0 , -1]
    ]
  end 

  def hisha_moveable
    [
      [1,0],
      [0,1],
      [-1,0],
      [0,-1]
    ]
  end

  def kaku_moveable
    [
      [1,1],
      [-1,1],
      [1,-1],
      [-1,-1]
    ]
  end

  def vector
    @direction == :forward ? 1 : -1
  end
end

class Player
  attr_reader :camp

  def initialize(camp)
    @camp = camp
    @pals = []
  end

  def has?(man)
    @camp == man.direction
  end

  def pal_with(man)
    @pals << man unless man.nil?
  end

  def sortie!(man)
    return nil unless @pals.map(&:name).include?(man)
    @pals.delete_at(@pals.map(&:name).index(man))
  end

  def beated?
    @pals.map(&:name).include?(:o)
  end

  def disp_pals
    @pals.map(&:dispname)
  end
end

class SyogiPlay
  def initialize
    @syogi = Syogi.new
  end
  def play
    puts <<-EOS.gsub(/^\s+/,'')
    #{@syogi.disp_pals}
    EOS
    puts ""

    @syogi.display
    puts ""
    puts "【#{@syogi.player? == :forward ? "先手(↑)" : "後手(↓)"}】の手番です"
    puts <<-EOS.gsub(/^\s+/,'')
    ご指示を。
    [0]打つ　[9]持ち駒を使う　[-1]投了
    EOS
    option(gets.chomp.to_i)
  end

  def display
  end

  def option(inp)
    return unless inp.kind_of?(Integer)
    correct_input = case inp
    when 0
      move
    when 9
      sortie
    when -1
      end_game
    end
    @syogi.switch if correct_input
    play
  end

  def move
    puts "どの駒を動かしますか。"
    puts "タテ ヨコ　の順に入力してください。"
    x,y = gets.chomp.split.map(&:to_i)
    @syogi.activate_man(x,y)
    puts "どこへ動かしますか。"
    puts "タテ ヨコ　の順に入力してください。"
    x,y = gets.chomp.split.map(&:to_i)
    @syogi.move!(x,y)
  end

  def sortie
    puts "#{@syogi.disp_pals}"
    puts "置きたいコマのシンボル名を入力してください。"
    man = gets.chomp.to_sym
    p man
    puts "どこへ置きますか"
    puts "タテ ヨコ　の順に入力してください。"
    x,y = gets.chomp.split.map(&:to_i)
    @syogi.sortie_man(x,y,man)
  end

  def end_game
    exit
  end
end
a = SyogiPlay.new
a.play
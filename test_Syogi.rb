require_relative "./Syogi.rb"
require "minitest/autorun"
class SyogiTest < MiniTest::Unit::TestCase
  def setup
    @test = Syogi.new
  end

  def test_has_man?
    check = @test.has_man?(1,2)
    assert_equal(false , check)
  end

  def test_moveable_cell
    for i in 0...9
      check = [@test.moveable_cell(2,i) , @test.moveable_cell(6,i)]
      assert_equal([[[3,i]] , [[5,i]]] , check)
    end
  end

  def test_place_init
    @test.place_init
    for i in 0...9
      check = [@test.has_man?(0,i) , @test.has_man?(8,i)]
      assert_equal([SyogiDef.base_line[0][i].name , SyogiDef.base_line[0][i].name] , check)
    end
    check = [
      @test.has_man?(1,1),
      @test.has_man?(7,7),
      @test.has_man?(1,7),
      @test.has_man?(7,1),
    ]
    assert_equal(%i/hisha hisha kaku kaku/ , check)
    for i in 0...9
      check = [@test.has_man?(2,i) , @test.has_man?(6,i)]
      assert_equal(%i/fu fu/,check)
    end
  end

  def test_keima_moveable
    check = []

    check << @test.moveable_cell(0,1)
    expected_res = []
    expected_res << [
    ] 

    check << @test.moveable_cell(0,7)
    expected_res << [
    ] 

    check << @test.moveable_cell(8,1)
    expected_res << [
    ] 

    check << @test.moveable_cell(8,7)
    expected_res << [
    ] 

    assert_equal(expected_res,check)
  end

  def test_o_moveable
    check = []

    check << @test.moveable_cell(0,4)
    expected_res = []
    expected_res << [
      [1,5],
      [1,4],
      [1,3],
    ] 

    check << @test.moveable_cell(8,4)
    expected_res << [
      [7,3],
      [7,4],
      [7,5],
    ] 

    assert_equal(expected_res,check)
  end
  def test_hisha_moveable
    check = []
    exp = []
    @test.activate_man(1,1)
    check << @test.moveable?(1,4)
    exp << true
    assert_equal(exp,check)
  end

  def test_activate_man
    check = []
    exp = []

    check << @test.activated?(0,0)
    exp << false
    @test.activate_man(0,0)
    check << @test.activated?(0,0)
    exp << true

    assert_equal(exp,check)
  end

  def test_moveable?
    check = []
    exp = []
    @test.activate_man(0,0)
    check << @test.moveable?(1,0)
    exp << true
    for i in 2...SyogiDef.lines
      check << @test.moveable?(i,0)
      exp << false
    end

    assert_equal(exp,check)
  end

  def test_move!
    check = []
    exp = []
    @test.activate_man(0,0)
    check << @test.move!(1,0)
    exp << true

    assert_equal(exp,check)
  end

  def test_player_switching
    check = []
    exp = []
    check << @test.player?
    exp << :forward
    check << @test.switch
    exp << :backward

    assert_equal(exp,check)
  end

  def test_cant_move_opponents_man
    check = []
    exp = []
    check << @test.player?
    exp << :forward
    @test.activate_man(0,0)
    check << @test.moveable?(1,0)
    exp << true
    @test.activate_man(8,0)
    check << @test.moveable?(1,0)
    exp << false

    assert_equal(exp,check)
  end

  def test_pal
    check = []
    exp = []
    for i in 2..5 
      @test.activate_man(i,0)
      @test.move!(i+1,0)
    end
    check << @test.sortie_man(4,4,:fu)
    exp << true
    @test.activate_man(4,4)
    check << @test.moveable?(5,4)
    exp << true
    assert_equal(exp,check)
  end
end